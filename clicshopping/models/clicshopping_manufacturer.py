# -*- encoding: utf-8 -*-
##############################################################################
#
#    Odoo, Open Source Management Solution
#    This module copyright (C) 2014 e-imaginis
#    (http://www.clicshopping.org).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import orm, fields
from openerp.tools.translate import _

class clicshopping_manufacturer(orm.Model):
    _name = 'clicshopping.manufacturer'
    _rec_name = 'clicshopping_manufacturers_name'
    
    _columns = {
        'clicshopping_manufacturers_id': fields.integer('Brand manufacturer Id', size=5, help="Id manufacturer Brand table of ClicShopping must be unique"),
        'ClicShopping_manufacturers_save_to_catalog': fields.boolean('ClicShopping', help="Save the manufacturer inside ClicShopping"),
        'clicshopping_manufacturers_name': fields.char('Brand Name', size=70, help='Name of brand manufacturer.'),
        'clicshopping_manufacturers_url': fields.char('Brand Url', translate=True, size=70, help='Url of brand manufacturer.'),
        'clicshopping_partner_id': fields.many2one('res.partner','Partner',  help='Select a partner for this brand if it exists.',  ondelete='restrict'),
        'clicshopping_manufacturers_image': fields.binary('Brand logo'),
		'clicshopping_manufacturers_status': fields.boolean('Brand Manufacturer Status',  default='1', help="If a manufacturer brand is not active, it will not be displayed in the catalog"),
        'clicshopping_manufacturer_description': fields.text('Description', translate=True),
		'clicshopping_manufacturer_seo_title': fields.char('Brand manufacturer Seo title', translate=True, size=70, help="If it empty, default in ClicSshopping will be taken"),
		'clicshopping_manufacturer_seo_description': fields.char('Brand manufacturer Seo Description', translate=True, size=150, help="If it empty, default in ClicSshopping will be taken"),
		'clicshopping_manufacturer_seo_keyword': fields.text('Brand manufacturer Seo Keywords', translate=True, help="If it empty, default in ClicSshopping will be taken"),
    }


class product_template(orm.Model):
    _inherit = 'product.template'
    _columns = {
        'clicshopping_product_manufacturer_id': fields.many2one('clicshopping.manufacturer','Brand', help='Select a brand for this product.')
    }